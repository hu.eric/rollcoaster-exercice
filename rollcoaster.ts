import { createReadStream, ReadStream } from "fs";
import { ReadLine, createInterface } from "readline";

import Logger from "./logger";

class GroupPattern {
  pattern: string;
  round: number;
  revenue: number;
  lastCursor: number;

  constructor(
    pattern: string,
    round: number,
    revenue: number,
    lastCursor: number
  ) {
    this.pattern = pattern;
    this.round = round;
    this.revenue = revenue;
    this.lastCursor = lastCursor;
  }
}

class Rollcoaster {
  // File info
  filePath: string;
  fileStream: ReadStream;

  // Rules
  price: number = 1;
  seats: number;
  rounds: number;
  numberOfGroups: number;
  groups: Array<number> = [];
  groupCursor: number = 0;

  groupPatternHistory: Map<string, GroupPattern> = new Map<
    string,
    GroupPattern
  >();
  revenue: number = 0;

  constructor(filePath: string) {
    Logger.info(`Creating ReadStream for ${filePath}`);
    this.filePath = filePath;
    this.fileStream = createReadStream(filePath);
    this.fileStream.on("error", (e) => {
      Logger.error(e);
    });
  }

  async initialize(): Promise<void> {
    const rl = createInterface({
      input: this.fileStream,
      crlfDelay: Infinity,
    });

    for await (const line of rl) {
      if (!this.seats && !this.rounds && !this.numberOfGroups) {
        const parameters: string[] = line.split(" ");
        this.seats = parseInt(parameters[0]);
        this.rounds = parseInt(parameters[1]);
        this.numberOfGroups = parseInt(parameters[2]);
      } else {
        this.groups.push(parseInt(line));
      }
    }
    Logger.info(
      `Rollcoaster initialized: ${this.seats} seats for ${this.rounds} rounds per day with ${this.numberOfGroups} groups.`
    );
  }

  createGroupPattern(
    cursor: number,
    revenue: number,
    round: number
  ): GroupPattern {
    let occupiedSeats = 0;
    let groupSize = this.groups[cursor];
    let groups = [];
    while (occupiedSeats + groupSize <= this.seats) {
      occupiedSeats += this.groups[cursor];
      groups.push(cursor.toString());
      cursor += 1;

      // Resetting cursor
      if (cursor === this.numberOfGroups) {
        cursor = 0;
      }
      groupSize = this.groups[cursor];
    }
    revenue += occupiedSeats * this.price;

    const groupPattern = new GroupPattern(
      groups.join("-"),
      round,
      revenue,
      cursor
    );

    return groupPattern;
  }

  computeRevenue(currentRound: number = 0): number {
    for (let i = currentRound; i < this.rounds; i += 1) {
      let occupiedSeats = 0;
      let groupSize = this.groups[this.groupCursor];
      while (occupiedSeats + groupSize <= this.seats) {
        occupiedSeats += this.groups[this.groupCursor];
        this.groupCursor += 1;

        // Resetting cursor
        if (this.groupCursor === this.numberOfGroups) {
          this.groupCursor = 0;
        }
        groupSize = this.groups[this.groupCursor];
      }
      this.revenue += occupiedSeats * this.price;
    }

    return this.revenue;
  }

  compute(): void {
    // We try to find a pattern
    const lastGroupPattern = this.createHistory();

    let currentRound = lastGroupPattern.round;
    this.revenue = lastGroupPattern.revenue;

    // If we still have round we continue calculating
    if (currentRound < this.rounds) {
      const multiplier = Math.trunc(this.rounds / currentRound);
      currentRound *= multiplier;
      this.revenue *= multiplier;
      this.groupCursor = lastGroupPattern.lastCursor;

      // Naive iteration to get the last rounds
      this.computeRevenue(currentRound);
    }
  }

  // Store each groups into a history map
  createHistory(): GroupPattern | undefined {
    let round = 1;
    let cursor = 0;
    let revenue = 0;
    let pattern;
    let lastGroupPattern;

    while (round !== this.rounds) {
      const groupPattern = this.createGroupPattern(cursor, revenue, round);

      pattern = groupPattern.pattern;
      if (!this.groupPatternHistory.has(groupPattern.pattern)) {
        this.groupPatternHistory.set(pattern, groupPattern);
        round += 1;
        cursor = groupPattern.lastCursor;
        revenue = groupPattern.revenue;
        lastGroupPattern = groupPattern;
      } else {
        break;
      }
    }

    return lastGroupPattern;
  }
}

export default Rollcoaster;
