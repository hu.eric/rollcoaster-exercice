import RollCoaster from "./rollcoaster";

async function start(): Promise<void> {
  const args = process.argv.slice(2);
  const rollcoaster = new RollCoaster(args[0]);

  await rollcoaster.initialize();

  rollcoaster.compute();

  console.log(rollcoaster.revenue);
}

start();
